package gestion.db;

import java.sql.Statement;

public class InsertToDB {
	
	public InsertToDB(String query) {
		try{
			Statement st = MysqlConnection.connection.createStatement();
						
		    st.executeUpdate(query);
		}catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}
}
