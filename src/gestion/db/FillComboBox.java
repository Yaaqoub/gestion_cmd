package gestion.db;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;

public class FillComboBox extends DefaultComboBoxModel{
	
	DefaultComboBoxModel list;
	
	//String[] nom = new String[10];
	
	ArrayList nom = new ArrayList();
	
	public FillComboBox() {
		
	}
	
	public DefaultComboBoxModel fillCombo(String query) {
		try{
			Statement st = MysqlConnection.connection.createStatement();
			ResultSet rs = st.executeQuery(query);
			
			int i = 0;
			
		    while (rs.next()) {
		    	nom.add(rs.getString("nom"));
		    	//rs.getInt("id");
		    	i++;
		    }
		}catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
		
		list = new DefaultComboBoxModel(nom.toArray());
		
		return list;
	}
}
