package gestion.db;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;

public class FillJTable {
	
	
	private Vector<String> columnNames;
	private Vector<Vector<Object>> data;
	
	
	public FillJTable() {
		
	}

	public DefaultTableModel fillTable(String query) {
		try{
			Statement st = MysqlConnection.connection.createStatement();
			ResultSet rs = st.executeQuery(query);
			
			ResultSetMetaData metaData = rs.getMetaData();
			
			// names of columns
		    columnNames = new Vector<String>();
		    int columnCount = metaData.getColumnCount();
		    for (int column = 1; column <= columnCount; column++) {
		        columnNames.add(metaData.getColumnName(column));
		    }
		    
		    // data of the table
		    data = new Vector<Vector<Object>>();
		    while (rs.next()) {
		        Vector<Object> vector = new Vector<Object>();
		        for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
		            vector.add(rs.getObject(columnIndex));
		        }
		        data.add(vector);
		    }
		}catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
		
		return new DefaultTableModel(data, columnNames);
	}
}
