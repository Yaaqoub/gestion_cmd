package gestion.ui;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import gestion.db.FillComboBox;
import gestion.db.InsertToDB;

public class NewClient {
	
	private JTextField civilite;
	private JTextField nom;
	private JTextField prenom;
	private JTextField email;
	private JTextField adresse;
	private JComboBox type;
	private JTextField numTel;
	private JTextField numFax;
	
	String[] buttons = {"Valider", "Reset", "Retour", "Quitter"};
	
	private FillComboBox fillC = new FillComboBox();
	
	
	public NewClient(JFrame frameC) {
		JFrame frame = new JFrame();
		JPanel panel = new JPanel(new BorderLayout(5, 5));
		JPanel label = new JPanel(new GridLayout(0, 1, 2, 15));
	    label.add(new JLabel("civilite", SwingConstants.RIGHT));
	    label.add(new JLabel("nom", SwingConstants.RIGHT));
	    label.add(new JLabel("prenom", SwingConstants.RIGHT));
	    label.add(new JLabel("email", SwingConstants.RIGHT));
	    label.add(new JLabel("adresse", SwingConstants.RIGHT));
	    label.add(new JLabel("type", SwingConstants.RIGHT));
	    label.add(new JLabel("numTel", SwingConstants.RIGHT));
	    label.add(new JLabel("numFax", SwingConstants.RIGHT));
	    
	    panel.add(label, BorderLayout.WEST);
		JPanel controls = new JPanel(new GridLayout(0, 1, 2, 2));
		
		civilite = new JTextField();
	    controls.add(civilite);
	    civilite.setText("");
	    
	    nom = new JTextField();
	    controls.add(nom);
	    nom.setText("");
	    
	    prenom = new JTextField();
	    controls.add(prenom);
	    prenom.setText("");
	    
	    email = new JTextField();
	    controls.add(email);
	    email.setText("");
	    
	    adresse = new JTextField();
	    controls.add(adresse);
	    adresse.setText("");
	    
	    type = new JComboBox();
	    controls.add(type);
	    type.setModel(fillC.fillCombo("SELECT * FROM `per_type`"));
	    
	    numTel = new JTextField();
	    controls.add(numTel);
	    numTel.setText("");
	    
	    numFax = new JTextField();
	    controls.add(numFax);
	    numFax.setText("");
	    
	    panel.add(controls, BorderLayout.CENTER);
		int btnValue = JOptionPane.showOptionDialog(frame, panel, "Ajouter nouveau client", 0, JOptionPane.PLAIN_MESSAGE, null, buttons, buttons[3]);
				
		if(btnValue == 0) {
			//System.out.println(type.getSelectedItem().toString());
			String query = "INSERT INTO `client`(`nom`, `prenom`, `adresse`, `civilite`, `client_type_id`, `num_tel`, `num_fax`, `email`) "
					+ "VALUES ('" + nom.getText() + "', '" + prenom.getText() + "', '" + adresse.getText() + "', '" + civilite.getText() + "', "
							+ "(SELECT id FROM `per_type` WHERE nom = '" + type.getSelectedItem().toString() + "'), '" + numTel.getText() + "',"
									+ "'" + numFax.getText() + "', '" + email.getText() + "')";
			
			new InsertToDB(query);
			
			frameC.dispose();
			new Client(frameC);
		}
		
		if(btnValue == 1) {
			new NewClient(frameC);
		}
		
		if(btnValue == 3) {
			System.exit(0);
		}
	}
}
