package gestion.ui;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import gestion.db.FillComboBox;
import gestion.db.InsertToDB;

public class NewArticle {
	
	private JComboBox nom_Fournisseur;
	private JTextField date_cmd;
	private JComboBox article_type;
	private JTextField prix_ht;
	private JTextField quantite;
	private JComboBox delai_paiement;
	private JTextField date_livraison;
	
	String[] buttons = {"Valider", "Reset", "Retour", "Quitter"};
		
	
	public NewArticle(JFrame frameC) {
		JFrame frame = new JFrame();
		JPanel panel = new JPanel(new BorderLayout(5, 5));
		JPanel label = new JPanel(new GridLayout(0, 1, 2, 15));
	    label.add(new JLabel("Nom Fournisseur", SwingConstants.RIGHT));
	    label.add(new JLabel("Date Cmd", SwingConstants.RIGHT));
	    label.add(new JLabel("Article", SwingConstants.RIGHT));
	    label.add(new JLabel("Prix HT", SwingConstants.RIGHT));
	    label.add(new JLabel("Quantite", SwingConstants.RIGHT));
	    label.add(new JLabel("Delai Paiement", SwingConstants.RIGHT));
	    label.add(new JLabel("Date Livraison", SwingConstants.RIGHT));
	    
	    panel.add(label, BorderLayout.WEST);
		JPanel controls = new JPanel(new GridLayout(0, 1, 2, 2));
		
		nom_Fournisseur = new JComboBox();
	    controls.add(nom_Fournisseur);
	    nom_Fournisseur.setModel(new FillComboBox().fillCombo("SELECT * FROM `fournisseur`"));
	    
	    date_cmd = new JTextField();
	    controls.add(date_cmd);
	    date_cmd.setText("");
	    
	    article_type = new JComboBox();
	    controls.add(article_type);
	    article_type.setModel(new FillComboBox().fillCombo("SELECT * FROM `type_article`"));
	    
	    prix_ht = new JTextField();
	    controls.add(prix_ht);
	    prix_ht.setText("");
	    
	    quantite = new JTextField();
	    controls.add(quantite);
	    quantite.setText("");
	    
	    delai_paiement = new JComboBox();
	    controls.add(delai_paiement);
	    delai_paiement.setModel(new FillComboBox().fillCombo("SELECT * FROM `delai_paiement`"));
	    
	    date_livraison = new JTextField();
	    controls.add(date_livraison);
	    date_livraison.setText("");
	    
	    panel.add(controls, BorderLayout.CENTER);
		int btnValue = JOptionPane.showOptionDialog(frame, panel, "Ajouter nouveau Article", 0, JOptionPane.PLAIN_MESSAGE, null, buttons, buttons[3]);
				
		if(btnValue == 0) {
			String query = "INSERT INTO `article`(`fournisseur_id`, `date_cmd`, `type_article_id`, `prix_ht`, `quantite`, `delai_paiement_id`, `date_livraison`) "
					+ "VALUES ((SELECT id FROM `fournisseur` WHERE nom = '" + nom_Fournisseur.getSelectedItem().toString() + "'), '" + date_cmd.getText() + "', "
							+ "(SELECT id FROM `type_article` WHERE nom = '" + article_type.getSelectedItem().toString() + "'), '" + prix_ht.getText() + "',"
									+ "'" + quantite.getText() + "', (SELECT id FROM `delai_paiement` WHERE nom = '" + delai_paiement.getSelectedItem().toString() + "'), '" + date_livraison.getText() + "')";
			
			new InsertToDB(query);
			
			frameC.dispose();
			new Article(frameC);
		}
		
		if(btnValue == 1) {
			new NewArticle(frameC);
		}
		
		if(btnValue == 3) {
			System.exit(0);
		}
	}
}
