package gestion.ui.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import gestion.ui.Article;
import gestion.ui.Bateau;
import gestion.ui.Client;
import gestion.ui.Fournisseur;
import gestion.ui.LoginForm;

public class MainButtons {
	
	public static void adminButton(JButton adminBtn) {
		adminBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				new LoginForm();
			}
		});
	}
	
	public static void clientButton(JButton clientBtn, JFrame frame) {
		clientBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				new Client(frame);
				frame.setVisible(false);
			}
		});
	}
	
	public static void articleButton(JButton articleBtn, JFrame frame) {
		articleBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				new Article(frame);
				frame.setVisible(false);
			}
		});
	}
	
	public static void bateauButton(JButton bateauBtn, JFrame frame) {
		bateauBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				new Bateau(frame);
				frame.setVisible(false);
			}
		});
	}
	
	public static void fournisseurButton(JButton fournisseurBtn, JFrame frame) {
		fournisseurBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				new Fournisseur(frame);
				frame.setVisible(false);
			}
		});
	}
	
	public static void quitterButton(JButton quitterBtn) {
		quitterBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				System.exit(0);
			}
		});
	}
}
