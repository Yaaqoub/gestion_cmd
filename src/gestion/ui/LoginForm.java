package gestion.ui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import gestion.db.MysqlConnection;

public class LoginForm {
    private JTextField username;						
    private JTextField password;	
    
    private String query = "";
    
    public static boolean isConnected = false;
        
    public LoginForm() {
    	isConnected = false;
    	JFrame frame = new JFrame();
		JPanel panel = new JPanel(new BorderLayout(5, 5));
		JPanel label = new JPanel(new GridLayout(0, 1, 2, 15));
	    label.add(new JLabel("Username", SwingConstants.RIGHT));
	    label.add(new JLabel("Password", SwingConstants.RIGHT));
	    
	    panel.add(label, BorderLayout.WEST);
		JPanel controls = new JPanel(new GridLayout(0, 1, 2, 2));
		
		username = new JTextField();
	    controls.add(username);
	    username.setText("manal");
	    
	    password = new JTextField();
	    controls.add(password);
	    password.setText("manal");
	    
	    panel.add(controls, BorderLayout.CENTER);
		JOptionPane.showMessageDialog(frame, panel, "Login", JOptionPane.DEFAULT_OPTION);
		
		try {
			query = "SELECT * FROM `admin` WHERE `username` = '" + username.getText() + "' and `password` = '" + password.getText() + "'";
			
		    Statement st = MysqlConnection.connection.createStatement();
		    
		    ResultSet rs = st.executeQuery(query);
		    
		    if(rs.next()) {
		    	System.out.println("the login is good");
		    	isConnected = true;
		    } else {
		    	new LoginForm();
		    }
		    
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}
		
		
    }
}
