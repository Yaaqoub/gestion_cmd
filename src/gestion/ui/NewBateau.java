package gestion.ui;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import gestion.db.FillComboBox;
import gestion.db.InsertToDB;

public class NewBateau {
	
	private JComboBox nom_Client;
	private JTextField date_cmd;
	private JComboBox bateau_type;
	private JTextField prix_ht;
	private JTextField quantite;
	private JComboBox delai_paiement;
	private JTextField date_livraison;
	
	String[] buttons = {"Valider", "Reset", "Retour", "Quitter"};
		
	
	public NewBateau(JFrame frameC) {
		JFrame frame = new JFrame();
		JPanel panel = new JPanel(new BorderLayout(5, 5));
		JPanel label = new JPanel(new GridLayout(0, 1, 2, 15));
	    label.add(new JLabel("Nom client", SwingConstants.RIGHT));
	    label.add(new JLabel("Date Cmd", SwingConstants.RIGHT));
	    label.add(new JLabel("Bateau", SwingConstants.RIGHT));
	    label.add(new JLabel("Prix HT", SwingConstants.RIGHT));
	    label.add(new JLabel("Quantite", SwingConstants.RIGHT));
	    label.add(new JLabel("Delai Paiement", SwingConstants.RIGHT));
	    label.add(new JLabel("Date Livraison", SwingConstants.RIGHT));
	    
	    panel.add(label, BorderLayout.WEST);
		JPanel controls = new JPanel(new GridLayout(0, 1, 2, 2));
		
		nom_Client = new JComboBox();
	    controls.add(nom_Client);
	    nom_Client.setModel(new FillComboBox().fillCombo("SELECT * FROM `client`"));
	    
	    date_cmd = new JTextField();
	    controls.add(date_cmd);
	    date_cmd.setText("");
	    
	    bateau_type = new JComboBox();
	    controls.add(bateau_type);
	    bateau_type.setModel(new FillComboBox().fillCombo("SELECT * FROM `bateau_type`"));
	    
	    prix_ht = new JTextField();
	    controls.add(prix_ht);
	    prix_ht.setText("");
	    
	    quantite = new JTextField();
	    controls.add(quantite);
	    quantite.setText("");
	    
	    delai_paiement = new JComboBox();
	    controls.add(delai_paiement);
	    delai_paiement.setModel(new FillComboBox().fillCombo("SELECT * FROM `delai_paiement`"));
	    
	    date_livraison = new JTextField();
	    controls.add(date_livraison);
	    date_livraison.setText("");
	    
	    panel.add(controls, BorderLayout.CENTER);
		int btnValue = JOptionPane.showOptionDialog(frame, panel, "Ajouter nouveau Bateau", 0, JOptionPane.PLAIN_MESSAGE, null, buttons, buttons[3]);
				
		if(btnValue == 0) {
			String query = "INSERT INTO `bateau`(`client_id`, `date_cmd`, `bateau_type_id`, `prix_ht`, `quantite`, `delai_paiement_id`, `date_livraison`) "
					+ "VALUES ((SELECT id FROM `client` WHERE nom = '" + nom_Client.getSelectedItem().toString() + "'), '" + date_cmd.getText() + "', "
							+ "(SELECT id FROM `bateau_type` WHERE nom = '" + bateau_type.getSelectedItem().toString() + "'), '" + prix_ht.getText() + "', "
									+ "'" + quantite.getText() + "', (SELECT id FROM `delai_paiement` WHERE nom = '" + delai_paiement.getSelectedItem().toString() + "'), "
											+ "'" + date_livraison.getText() + "')";
			
			new InsertToDB(query);
			
			frameC.dispose();
			new Bateau(frameC);
		}
		
		if(btnValue == 1) {
			new NewBateau(frameC);
		}
		
		if(btnValue == 3) {
			System.exit(0);
		}
	}
}
