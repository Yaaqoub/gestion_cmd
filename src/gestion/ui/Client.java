package gestion.ui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import gestion.db.FillJTable;
import gestion.db.InsertToDB;

public class Client extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 557616669173883616L;
	
	private JTable table;
	
	private JButton quitter;
	private JButton retour;
	private JButton nouveau;
	private JButton supprimer;
	
	private String value = "";
	
	private FillJTable fillTable = new FillJTable();

	public Client(JFrame frame) {
		init();
		createTable();
		createButtons();
		addListener(frame, this);
	}
	
	private void init() {
		this.setSize(700, 600); 						
		this.setLocationRelativeTo(null); 			
		this.setTitle("Gestion de commandes"); 
		this.setResizable(false);		 				
		this.setLayout(null);
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		this.setVisible(true);
	}
	
	private void createTable() {
		table = new JTable();
		table.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 0, Color.GRAY));
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(20, 10, 650, 250);
		this.getContentPane().add(scrollPane);
		
		table.setModel(fillTable.fillTable("SELECT * FROM `client`"));
		repaint();
        revalidate();
	}
	
	private void createButtons() {
		nouveau = new JButton("Nouveau");
		nouveau.setBounds(40, 500, 150, 30);
		nouveau.setBackground(Color.LIGHT_GRAY);
		if (LoginForm.isConnected) {
			nouveau.setEnabled(true);
		} else {
			nouveau.setEnabled(false);
		}
		nouveau.setOpaque(true);
    	this.add(nouveau);
    	
    	supprimer = new JButton("Supprimer");
    	supprimer.setBounds(200, 500, 150, 30);
    	supprimer.setBackground(Color.LIGHT_GRAY);
		if (LoginForm.isConnected) {
			supprimer.setEnabled(true);
		} else {
			supprimer.setEnabled(false);
		}
		supprimer.setOpaque(true);
    	this.add(supprimer);
    	
    	retour = new JButton("Retour");
    	retour.setBounds(360, 500, 150, 30);
    	retour.setBackground(Color.LIGHT_GRAY);
    	retour.setOpaque(true);
    	this.add(retour);
    	
    	quitter = new JButton("Quitter");
    	quitter.setBounds(520, 500, 150, 30);
    	quitter.setBackground(Color.LIGHT_GRAY);
    	quitter.setOpaque(true);
    	this.add(quitter);
	}
	
	private void addListener(JFrame frame, JFrame thisFrame) {
		retour.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				frame.setVisible(true);
				thisFrame.setVisible(false);
			}
		});
		
		quitter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				System.exit(0);
			}
		});
		
		nouveau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				new NewClient(thisFrame);
			}
		});
		
		table.addMouseListener(new java.awt.event.MouseAdapter() {
		    @Override
		    public void mouseClicked(java.awt.event.MouseEvent e) {
		        int column = 0;
		        int row = table.getSelectedRow();
		        value = table.getModel().getValueAt(row, column).toString();
		    }
		});
		
		supprimer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				String query = "DELETE FROM `client` WHERE id = " + value;

				new InsertToDB(query);
				
				thisFrame.dispose();
				new Client(thisFrame);
			}
		});
	}
}
