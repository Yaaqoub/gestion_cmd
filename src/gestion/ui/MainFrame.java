package gestion.ui;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JFrame;

import gestion.db.MysqlConnection;
import gestion.ui.listener.MainButtons;

public class MainFrame extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8013570426437368344L;
	
	private JButton admin_button = new JButton("Admin");
	
	private JButton client_button = new JButton("Client");
	
	private JButton article_button = new JButton("Article");
	
	private JButton fournisseur_button = new JButton("Fournisseur");
	
	private JButton bateau_button = new JButton("Bateau");
	
	private JButton quitter_button = new JButton("Quitter");
	
	
	public MainFrame() {
		new MysqlConnection();
		init();
		addListener();
	}
	
	private void init() {
		this.setTitle("Gestion de Commandes");
		
		this.setPreferredSize(new Dimension(400, 500));
		//this.setMinimumSize(new Dimension(700, 600));
		this.setResizable(false);
		
		this.pack();
		this.setLocationRelativeTo(null);
		
		addButtons();
	}
	
	private void addButtons() {
		this.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.insets = new Insets(10,0,0,0);
		
		admin_button.setPreferredSize(new Dimension(200, 50));
		client_button.setPreferredSize(new Dimension(200, 50));
		article_button.setPreferredSize(new Dimension(200, 50));
		fournisseur_button.setPreferredSize(new Dimension(200, 50));
		bateau_button.setPreferredSize(new Dimension(200, 50));
		quitter_button.setPreferredSize(new Dimension(200, 50));
		
		admin_button.setFont(new Font("Arial", Font.PLAIN, 25));
		client_button.setFont(new Font("Arial", Font.PLAIN, 25));
		article_button.setFont(new Font("Arial", Font.PLAIN, 25));
		fournisseur_button.setFont(new Font("Arial", Font.PLAIN, 25));
		bateau_button.setFont(new Font("Arial", Font.PLAIN, 25));
		quitter_button.setFont(new Font("Arial", Font.PLAIN, 25));
		
		this.add(admin_button, gbc);
		this.add(client_button, gbc);
		this.add(article_button, gbc);
		this.add(fournisseur_button, gbc);
		this.add(bateau_button, gbc);
		this.add(quitter_button, gbc);
	}
	
	private void addListener() {
		MainButtons.adminButton(admin_button);
		MainButtons.clientButton(client_button, this);
		MainButtons.articleButton(article_button, this);
		MainButtons.fournisseurButton(fournisseur_button, this);
		MainButtons.bateauButton(bateau_button, this);
		MainButtons.quitterButton(quitter_button);
	}
}
